/**
 *
 * Author:      Ebubekir Yigit
 *
 * ID:          21328629
 *
 * File:        binarysearcher.c
 *
 * Purpose:     The program finds all readable files and reads them.
 *              Adds all of words in Binary Search Tree.
 *              And reads commands file.
 *
 *              ADD command gives us a new file path. Program finds words and adds tree again.
 *
 *              SEARCH comamnd gives us a word. Program search word in tree.
 *
 *              REMOVE command gives us a word. Program removes word in tree.
 *
 *              PRINT command wants print all of Tree Nodes this format:
 *
 *                      <word>
 *                          <word count>
 *                          <dept level in tree>
 *                          <reads which text file> <count in text file>
 *                          <reads which text file> <count in text file>
 *                          .
 *                          .
 *                          .
 *
 *
 *
 * Compile:     gcc binarysearcher.c -o binarysearcher
 *
 * Run:         ./binarysearcher ./input_dir command.txt
 *
 *
 * Input:       none
 *
 * Output:
 *
 *
 *
 * Notes:
 *              All files reads char by char.
 *              Program works all platforms.
 *              libgen.h library added for basename() function
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <libgen.h>


#define MAX_FILE_NAME 500   /* in dev.cs  file name's max char limit almost 250      in windows max char limit almost 230-260 */

#define PRINT "PRINT\0"
#define SEARCH "SEARCH\0"
#define ADD "ADD\0"
#define REMOVE "REMOVE\0"


typedef struct {
    char *string;
    int size;
    int top;
}String;

typedef struct {
    char **keys;
    int *values;
    int size;
    int top;
}HashMap;

struct Node{
    struct Node *left;
    struct Node *right;
    String word;
    HashMap files;
};

typedef struct {
    String fileName;
    String filePath;
}File;

typedef struct {
    File *files;
    int size;
    int top;
}FileArray;

/**********   Simple String library functions (auto increased size)  ************/
void initializeString(String *string, int initialSize);
void concatString(String *string, char *newCharArray);
void clearString(String *string);
void copyString(String *string, char *newCharArray);

/**********   File ArrayList<File> library   ***********/
void initializeFileArray(FileArray *fileArray, int initialSize);
void initializeFile(File *file, int initialSize);
void addFileArray(FileArray *fileArray, File newFile);
void findFiles(char *dirName,FileArray *fileArray);
void findAllWords(char *filePath, char *fileName, struct Node *rootPointer);

/**********  Simple HashMap<String><Integer> library  **********/
int containsHashMap(HashMap *hashMap, char *string);
void initializeHashMap(HashMap *hashMap, int initialSize);
void addHashMap(HashMap *hashMap,char* key, int value);

/**********   Command Parsers and Processers   **********/
void addCommand(FILE *outputFile, char *line, struct Node *rootPointer);
void removeCommand(FILE *outputFile, char *line, struct Node *rootPointer);
void searchCommand(FILE *outputFile, char *line, struct Node *rootPointer);
void printCommand(FILE *outputFile, char *line, struct Node *rootPointer);
void parseCommands(FILE *outputFile, char *line, struct Node *rootPointer) ;
void readCommands(FILE *outputFile, char *fileName, struct Node *rootPointer);

/**********   Binary Search Tree functions   *********/
void printTree(FILE *outputFile, struct Node *rootPointer,struct Node *root,int type);
void printTreeFormat(FILE *outputFile, struct Node *rootPointer, struct Node *root);
struct Node* searchElement(struct Node *node, char *key);
struct Node* addNewNode(struct Node *node, char *newWord, char *whichTextFile);
struct Node* deleteNode(struct Node *node, char* key);
struct Node* getNewNode();
int getLevel(struct Node *node, char *data, int level);

/**********   Util Functions   *********/
int isReadableFile(char *filePath);
char* getLowerCase(char *string);
int fileType(char *path);
int isDigit(char p) ;
int isAlphabetic(char p);
int alphabeticOrder (char *a, char *b);


int main(int argc, char* argv[]) {

    /* root pointer points main Root pointer*/
    struct Node *rootPointer = getNewNode();
    copyString(&rootPointer->word," \0");

    /* contains all founded readable files */
    FileArray fileArray;
    initializeFileArray(&fileArray,20);

    /* inserts founded files filearray */
    findFiles(argv[1],&fileArray);

    int i;
    for (i = 0; i <= fileArray.top; i++) {
        if (isReadableFile(fileArray.files[i].filePath.string)) {
            /* reads files and adds words tree*/
            findAllWords(fileArray.files[i].filePath.string, fileArray.files[i].fileName.string, rootPointer);
        }
    }

    FILE *outputFile = fopen("output.txt","w");

    /* runs commands */
    readCommands(outputFile,argv[2],rootPointer);

    fclose(outputFile);

    return 0;

}
/**
 * reads commands and sends command parser
 * @param outputFile print out file pointer
 * @param fileName commands file
 * @param rootPointer Binary tree root pointer
 */
void readCommands(FILE *outputFile, char *fileName, struct Node *rootPointer){
    FILE *file = fopen(fileName, "r");
    int c;

    String command;
    initializeString(&command,5);

    while ((c = fgetc(file)) != EOF) {
        if (c != '\n' && (isAlphabetic((char)c) || isDigit((char)c) || (char)c == ' ' || (char)c == '/' || (char)c == '.')) {
            char temp[2];
            temp[0] = (char) c;
            temp[1] = '\0';
            concatString(&command, temp);
        }
        else {
            if (command.top != -1) {
                concatString(&command, "\0");
                parseCommands(outputFile,command.string,rootPointer);
                clearString(&command);
            }
        }
    }
    if (command.top != -1){
        concatString(&command,"\0");
        parseCommands(outputFile,command.string,rootPointer);
        clearString(&command);
    }

    fclose(file);

}

/**
 * parse commands and sends corresponding functions
 * @param outputFile print out file pointer
 * @param line command file's a line
 * @param rootPointer rootPointer Binary tree root pointer
 */
void parseCommands(FILE *outputFile, char *line, struct Node *rootPointer) {
    String command;
    initializeString(&command, 100);
    copyString(&command, line);

    char *token = strtok(command.string, " ");

    if (strcmp(token, ADD) == 0) {
        addCommand(outputFile,line,rootPointer);
    } else if (strcmp(token, REMOVE) == 0) {
        removeCommand(outputFile,line,rootPointer);
    } else if (strcmp(token, SEARCH) == 0) {
        searchCommand(outputFile,line,rootPointer);
    } else if (strcmp(token, PRINT) == 0) {
        printCommand(outputFile,line,rootPointer);
    }
}

/**
 * if given file is readable, reads file and adds words in tree
 * @param outputFile print out file pointer
 * @param line command file's a line
 * @param rootPointer rootPointer Binary tree root pointer
 */
void addCommand(FILE *outputFile, char *line, struct Node *rootPointer){
    String command;
    initializeString(&command, 100);
    copyString(&command, line);

    printf("%s\n",line);
    fprintf(outputFile,"%s\n",line);

    char *token = strtok(command.string, " ");
    token = strtok(NULL," ");

    if (isReadableFile(token)) {
        findAllWords(token, basename(token), rootPointer);
    }
}
/**
 * searchs tree for given word (if word is not found printed only command)
 * @param outputFile print out file pointer
 * @param line command file's a line
 * @param rootPointer rootPointer Binary tree root pointer
 */
void searchCommand(FILE *outputFile, char *line, struct Node *rootPointer){
    String command;
    initializeString(&command, 100);
    copyString(&command, line);

    printf("%s\n",SEARCH);
    fprintf(outputFile,"%s\n",SEARCH);

    char *token = strtok(command.string, " ");
    token = strtok(NULL," ");

    struct Node *foundedElement = searchElement(rootPointer,token);

    if (foundedElement != NULL){
        printTreeFormat(outputFile,rootPointer,foundedElement);
    }
}

/**
 * deletes tree's node which contains given word (if key is not founded printed only command)
 * @param outputFile print out file pointer
 * @param line command file's a line
 * @param rootPointer rootPointer Binary tree root pointer
 */
void removeCommand(FILE *outputFile, char *line, struct Node *rootPointer){
    String command;
    initializeString(&command, 100);
    copyString(&command, line);

    printf("%s\n",line);
    fprintf(outputFile,"%s\n",line);


    char *token = strtok(command.string, " ");
    token = strtok(NULL," ");


    rootPointer = deleteNode(rootPointer,token);
}

/**
 * Print all of tree Nodes given format (if ASC -> alphabetic order,  if DSC -> reverse alphabetic order)
 * @param outputFile print out file pointer
 * @param line command file's a line
 * @param rootPointer rootPointer Binary tree root pointer
 */
void printCommand(FILE *outputFile, char *line, struct Node *rootPointer){
    String command;
    initializeString(&command, 100);
    copyString(&command, line);

    char *token = strtok(command.string, " ");
    token = strtok(NULL, " ");
    token = strtok(NULL, " ");

    if (token == NULL){
        printf("%s\n",line);
        fprintf(outputFile,"%s\n",line);
        printTree(outputFile,rootPointer,rootPointer,2);
    }
    else if (strcmp(token,"ASC") == 0){
        printf("%s\n",line);
        fprintf(outputFile,"%s\n",line);
        printTree(outputFile,rootPointer,rootPointer,0);
    }
    else if (strcmp(token,"DSC") == 0){
        printf("%s\n",line);
        fprintf(outputFile,"%s\n",line);
        printTree(outputFile,rootPointer,rootPointer,1);
    }

}



/**
 * Finds all files in given directory name
 * @param dirName directory name
 * @param fileArray ArrayList<File>
 */
void findFiles(char *dirName,FileArray *fileArray)
{
    DIR *dir;
    struct dirent *entry;

    if (!(dir = opendir(dirName)))
        return;
    if (!(entry = readdir(dir)))
        return;

    while ((entry = readdir(dir))){
        String filePath;
        initializeString(&filePath,500);
        copyString(&filePath,dirName);
        concatString(&filePath,"/\0");
        concatString(&filePath,entry->d_name);

        /* a directory */
        if (fileType(filePath.string) == 1) {

            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;

            findFiles(filePath.string,fileArray);
        }
            /* a file */
        else if (fileType(filePath.string) == 0){

            File file;
            initializeFile(&file,100);

            copyString(&file.fileName,entry->d_name);
            copyString(&file.filePath,filePath.string);

            addFileArray(fileArray,file);

        }
    }
    closedir(dir);
}

/**
 * Finds all words and adds Binary Search Tree
 * @param filePath file absolute path
 * @param fileName file base name
 * @param rootPointer Binary tree root pointer
 */
void findAllWords(char *filePath, char *fileName, struct Node *rootPointer){

    FILE *file = fopen(filePath, "r");
    String oneWord;
    int c;

    initializeString(&oneWord,20);

    int flag = 0;

    while ((c = fgetc(file)) != EOF) {
        if (isDigit((char) c) || isAlphabetic((char) c) || (char) c == '-' || (char) c == '\'') {
            flag = 1;
            char temp[2];
            temp[0] = (char) c;
            temp[1] = '\0';
            concatString(&oneWord,temp);
        }
        else {
            if (flag == 1){

                concatString(&oneWord,"\0");

                rootPointer = addNewNode(rootPointer,oneWord.string,fileName);

                clearString(&oneWord);
            }
            flag = 0;
        }
    }
    if (oneWord.top != -1){

        concatString(&oneWord,"\0");

        rootPointer = addNewNode(rootPointer,oneWord.string,fileName);

        clearString(&oneWord);
    }

    fclose(file);
}




/**************************************    UTIL FUNCTIONS   ************************************************/

/**
 * File initializer
 * @param file file structure
 * @param initialSize size must be positive
 */
void initializeFile(File *file, int initialSize){
    initializeString(&file->fileName,initialSize);
    initializeString(&file->filePath,initialSize);
}

/**
 * File ArrayList initializer
 * @param fileArray ArrayList<File>
 * @param initialSize initialSize size must be positive
 */
void initializeFileArray(FileArray *fileArray, int initialSize){
    fileArray->files = (File*)malloc(initialSize * sizeof(File));
    fileArray->top = -1;
    fileArray->size = initialSize;
}

/**
 * adds a file in Files array (auto increased size)
 * @param fileArray ArrayList<File>
 * @param newFile new File
 */
void addFileArray(FileArray *fileArray, File newFile){
    if (fileArray->top == fileArray->size - 1){
        fileArray->size *= 2;
        fileArray->files = (File*)realloc(fileArray->files,fileArray->size * sizeof(File));
    }
    fileArray->top++;
    fileArray->files[fileArray->top] = newFile;
}

/**
 * Char is digit or something else
 * @param p given char
 * @return returns 1 if char is digit else returns 0
 */
int isDigit(char p) {
    if (p >= '0' && p <= '9') {
        return 1;
    }
    else {
        return 0;
    }
}

/**
 * Char is alphabetic or something else
 * @param p given char
 * @return returns 1 if char is alphabetic else returns 0
 */
int isAlphabetic(char p){
    if ((p >= 'A' && p <= 'Z') || (p >= 'a' && p <= 'z')){
        return 1;
    }
    else {
        return 0;
    }
}

/**
 * initialize a Node and return it
 * @return a Node
 */
struct Node* getNewNode(){
    struct Node *node = (struct Node*)malloc(sizeof(struct Node));
    initializeHashMap(&node->files,10);
    node->left = NULL;
    node->right = NULL;
    initializeString(&node->word,50);
    return node;
}

/**
 * Gives us given word's depth level in tree
 * @param node root pointer
 * @param data wanted word
 * @param level tree level
 * @return returns level if Node exist, else returns 0
 */
int getLevel(struct Node *node, char *data, int level)
{
    if (node == NULL) {
        return 0;
    }

    if (strcmp(node->word.string,data) == 0) {
        return level;
    }

    int downlevel = getLevel(node->left, data, level + 1);
    if (downlevel != 0) {
        return downlevel;
    }

    downlevel = getLevel(node->right, data, level + 1);
    return downlevel;
}

/**
 * adds new Node in tree
 * @param node root pointer
 * @param newWord new Keyword
 * @param whichTextFile key comes which text file?
 * @return returns inserted Node
 */
struct Node* addNewNode(struct Node *node, char *newWord, char *whichTextFile){

    if (node == NULL){
        struct Node *tempNode = getNewNode();
        concatString(&tempNode->word,newWord);
        addHashMap(&tempNode->files,whichTextFile,1);
        return tempNode;
    }

    if (alphabeticOrder(newWord,node->word.string) > 0){
        node->right = addNewNode(node->right,newWord,whichTextFile);
    }
    else if (alphabeticOrder(newWord,node->word.string) < 0){
        node->left = addNewNode(node->left,newWord,whichTextFile);
    }
    else {
        /* word is the same word*/
        addHashMap(&node->files,whichTextFile,1);
    }
    return node;
}

/**
 *
 * @param node root pointer
 * @param key wanted keyword
 * @return returns founded Node, (can be NULL)
 */
struct Node* searchElement(struct Node *node, char *key){
    if (node == NULL){
        return NULL;
    }
    if (alphabeticOrder(key,node->word.string) > 0){
        return searchElement(node->right,key);
    }
    else if (alphabeticOrder(key,node->word.string) < 0){
        return searchElement(node->left,key);
    }
    else {
        /* element found! */
        return node;
    }
}

/**
 * finds smallest Node in tree
 * @param node a Node
 * @return returns min Node
 */
struct Node* searchMinNode(struct Node *node){
    /* leftmost node */
    if (node == NULL){
        return NULL;
    }
    else if (node->left != NULL){
        return searchMinNode(node->left);
    }
    else {
        return node;
    }
}

/**
 * Deletes a Node which contains given keyword
 * @param node a Node
 * @param key keyword which we want to delete
 * @return
 */
struct Node* deleteNode(struct Node *node, char* key){
    struct Node *temp;
    if (node == NULL){
        /* key not found ! */
    }
    else if (alphabeticOrder(key,node->word.string) > 0){
        node->right = deleteNode(node->right,key);
    }
    else if (alphabeticOrder(key,node->word.string) < 0){
        node->left = deleteNode(node->left,key);
    }
    else {
        /* key found! deleting... */
        if (node->right != NULL && node->left != NULL){
            temp = searchMinNode(node->right);

            /* copy the words */
            char *tempWord = (char*)malloc(temp->word.size * sizeof(char));
            tempWord[0] = '\0';
            strcpy(tempWord,temp->word.string);

            clearString(&node->word);
            copyString(&node->word,tempWord);

            /* copy files hashMap */
            /******************************/
                node->files = temp->files;
            /******************************/

            /* deletes changed data */
            node->right = deleteNode(node->right,temp->word.string);
        }
        else {
            temp = node;
            if (node->left == NULL){
                node = node->right;
            }
            else if (node->right == NULL){
                node = node->left;
            }
        }
    }
    return node;
}

/**
 * print all of tree Nodes
 * @param outputFile print out output file
 * @param rootPointer Binary search tree root pointer
 * @param root searched Node
 * @param type print type (0 ASC, 1 DSC, 2 Pre-order)
 */
void printTree(FILE *outputFile, struct Node *rootPointer,struct Node *root,int type){

    if (root == NULL){
        return;
    }
    else {
        switch (type) {

                /* alphabetic order */
            case 0:
                printTree(outputFile,rootPointer,root->left,type);
                if (strcmp(root->word.string," \0") != 0){
                    printTreeFormat(outputFile,rootPointer,root);
                }
                printTree(outputFile,rootPointer,root->right,type);
                break;

                /* reverse alphabetic order */
            case 1:
                printTree(outputFile,rootPointer,root->right,type);
                if (strcmp(root->word.string," \0") != 0){
                    printTreeFormat(outputFile,rootPointer,root);
                }
                printTree(outputFile,rootPointer,root->left,type);
                break;

                /* preorder print */
            case 2:
                if (strcmp(root->word.string," \0") != 0){
                    printTreeFormat(outputFile,rootPointer,root);
                }
                printTree(outputFile,rootPointer,root->left,type);
                printTree(outputFile,rootPointer,root->right,type);
                break;
            default:
                printf("Invalid type of print tree function!");
        }
    }
}

/**
 * Prints a Node given format
 * @param outputFile print out output file
 * @param rootPointer Tree's root pointer
 * @param root given Node
 */
void printTreeFormat(FILE *outputFile, struct Node *rootPointer, struct Node *root){
    /* print a Node */
    int wordCounter = 0;
    int j;
    for (j = 0; j <= root->files.top; j++){
        wordCounter += root->files.values[j];
    }
    printf(" %s\n",getLowerCase(root->word.string));
    fprintf(outputFile," %s\n",root->word.string);
    printf("\t%d\n",wordCounter);
    fprintf(outputFile,"\t%d\n",wordCounter);
    printf("\t%d\n",getLevel(rootPointer,root->word.string,0));
    fprintf(outputFile,"\t%d\n",getLevel(rootPointer,root->word.string,0));
    int i;
    for (i = 0; i <= root->files.top; i++){
        printf("\t%s %d\n",root->files.keys[i],root->files.values[i]);
        fprintf(outputFile,"\t%s %d\n",root->files.keys[i],root->files.values[i]);
    }
}

/**
 * String initializer
 * @param string String structure
 * @param initialSize size must be positive
 */
void initializeString(String *string, int initialSize){
    string->string = (char*)malloc(initialSize * sizeof(char));
    string->string[0] = '\0';
    string->size = initialSize;
    string->top = -1;
}

/**
 * copies a string (auto increased size)
 * @param string
 * @param newCharArray
 */
void copyString(String *string, char *newCharArray){

    /* copies a char* begining of the string */

    int i;
    for (i = 0; newCharArray[i] != '\0'; i++){
        if (string->top == string->size -1){
            string->size *= 2;
            string->string = (char*)realloc(string->string,string->size * sizeof(char));
        }
        string->top++;
        string->string[i] = newCharArray[i];
    }
    if (string->top == string->size -1){
        string->size *= 2;
        string->string = (char*)realloc(string->string,string->size * sizeof(char));
    }
    string->string[i] = '\0';
}

/**
 * concats strings (size auto increased)
 * @param string
 * @param newCharArray
 */
void concatString(String *string, char *newCharArray){

    /* concats two strings */

    int i;
    for (i = 0; newCharArray[i] != '\0'; i++){
        if (string->top == string->size -1){
            string->size *= 2;
            string->string = (char*)realloc(string->string,string->size * sizeof(char));
        }
        string->top++;
        string->string[string->top] = newCharArray[i];
    }

    if (string->top == string->size -1){
        string->size *= 2;
        string->string = (char*)realloc(string->string,string->size * sizeof(char));
    }

    string->string[string->top+1] = '\0';
}

/**
 * Clear all chars in string but size is same
 * @param string
 */
void clearString(String *string){

    /* string's chars NULL char "reset string" */

    int i;
    for (i = 0; i <= string->top; i++){
        string->string[i] = '\0';
    }

    string->top = -1;
}

/**
 * HashMap initializer
 * @param hashMap
 * @param initialSize
 */
void initializeHashMap(HashMap *hashMap, int initialSize){
    hashMap->keys = (char**)malloc(initialSize * sizeof(char*));
    hashMap->values = (int*)malloc(initialSize * sizeof(int));
    hashMap->size = initialSize;
    hashMap->top = -1;

    int i;
    for (i = hashMap->top+1; i < hashMap->size; i++){
        hashMap->keys[i] = (char*)malloc(MAX_FILE_NAME * sizeof(char));
        hashMap->keys[i][0] = '\0';
    }
}

/**
 * adds HashMap a key (if key already exist, key's value++)
 * @param hashMap
 * @param key
 * @param value
 */
void addHashMap(HashMap *hashMap,char* key, int value) {

    /* if key not found add a key which has value 1, if a key found its value++ */
    if (hashMap->top == hashMap->size - 1) {
        hashMap->size *= 2;
        hashMap->keys = (char **) realloc(hashMap->keys, hashMap->size * sizeof(char *));
        hashMap->values = (int *) realloc(hashMap->values, hashMap->size * sizeof(int));

        int i;
        for (i = hashMap->top + 1; i < hashMap->size; i++) {
            hashMap->keys[i] = (char *) malloc(MAX_FILE_NAME * sizeof(char));
            hashMap->keys[i][0] = '\0';
        }
    }

    int k = containsHashMap(hashMap, key);
    if (k >= 0) {
        hashMap->values[k] += 1;
    } else {
        hashMap->top++;
        strcpy(hashMap->keys[hashMap->top], key);
        hashMap->values[hashMap->top] = value;
    }
}

/**
 * Controls HashMap contains this string
 * @param hashMap
 * @param string
 * @return if it contains return 1 else 0
 */
int containsHashMap(HashMap *hashMap, char *string){
    int i;
    int temp = -1;
    for (i = 0; i <= hashMap->top; i++){
        if (strcmp(hashMap->keys[i],string) == 0){
            temp = i;
            break;
        }
    }
    return temp;
}

/**
 * Dictionary order on strings
 * @param a
 * @param b
 * @return
 */
int alphabeticOrder (char *a, char *b) {
    /* which string comes first in dictionary */
    while (*a && *b) {
        if (tolower(*a) != tolower(*b)) {
            break;
        }
        ++a;
        ++b;
    }
    return tolower(*a) - tolower(*b);
}

/**
 * Controls char is uppercase
 * @param c
 * @return
 */
int isUpperCase(char c){
    if(c >= 'A' && c <= 'Z')
        return 1;
    else
        return 0;
}

/**
 * Converts char lowercase
 * @param c
 * @return
 */
int toLowerCase(char c){
    return c + (32);
}

/**
 * Convers lowercase all of chars in strings
 * @param string
 * @return
 */
char* getLowerCase(char *string){
    int len = strlen(string);
    int i;
    for (i = 0; i < len; i++){
        if (isUpperCase(string[i])){
            string[i] = (char)toLowerCase(string[i]);
        }
    }
    return string;
}

/**
 * Checks file type
 * @param path File absolute path
 * @return  returns -1 if file not exist
 *          returns 0 file type is "a file"
 *          returns 1 file type is "a directory"
 */
int fileType(char *path){

    /* returns -1 if file not exist */
    /* returns 0 file type is "a file" */
    /* returns 1 file type is "a directory" */
    int temp = -5;
    struct stat s;
    if( stat(path,&s) == 0 )
    {
        if( s.st_mode & S_IFDIR )
        {
            temp = 1;
        }
        else if( s.st_mode & S_IFREG )
        {
            temp = 0;
        }

    }
    else
    {
        temp = -1;
    }
    return temp;
}

/**
 * Search file's first 100 chars or EOF
 * @param filePath file absolute path
 * @return returns 1 if it is readable else returns 0
 */
int isReadableFile(char *filePath){
    int temp = 1;
    FILE *file = fopen(filePath,"r");

    int c;
    int counter = 0;

    if (file == NULL){
        return 0;
    }

    while ((c = fgetc(file)) != EOF && counter != 100)
    {
        if (c >= 0 && c <= 127){
            counter++;
        }
        else {
            temp = 0;
            break;
        }
    }

    fclose(file);

    return temp;
}

